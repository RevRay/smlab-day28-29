package ru.smlab.day28.kassa;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

public class CashierTest4 {
    Cashier cashier;

    @BeforeGroups({"positiveScenario"})
    public void setUpPaymentService(){
        System.out.println("### Осуществил подключение к платежной системе");
    }

    @Test(groups = "positiveScenario")
    public void testBoundary(){
        System.out.println("testBoundary");
        Customer c = new Customer(18, true);
        cashier = new Cashier();
        boolean actualResult = cashier.validateCustomer(c);

        Assert.assertTrue(actualResult);
    }

}
