package ru.smlab.day28.kassa;

import org.testng.Assert;
import org.testng.annotations.*;

public class CashierTest3 {
    //DRY

    Cashier cashier;

    @BeforeSuite
    public void globalPrepare(){
        System.out.println("before <suite> setup");
    }

    @AfterSuite
    public void globalRelease(){
        System.out.println("after <suite> setup");
    }

    @BeforeTest
    public void do1(){
        System.out.println("before <test> setup");
    }
    @AfterTest
    public void do2(){
        System.out.println("after <test> setup");
    }

    @BeforeGroups({"positiveScenario"})
    public void setUpPaymentService(){
        System.out.println("### Осуществил подключение к платежной системе");
    }

    @BeforeClass
    public void setUp() {
        System.out.println("@BeforeClass Осуществляем подготовительную работу");
        cashier = new Cashier();
    }

    @AfterClass
    public void tearDown() {
        System.out.println("@AfterClass Откатываю БД к прежнему состоянию");
    }

    @Test(
            groups = {"positiveScenario", "smoke", "uses-integration", "payment"},
            description = "Позитивный сценарий на валидацию клиента кассиром"
    )
    public void testPositive(){
        System.out.println("testPositive");
        //подготовка тестовых данных
        Customer c = new Customer(19, true);

        boolean actualResult = cashier.validateCustomer(c);
        boolean expectedResult = true;

//        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertTrue(actualResult);
    }

    @Test
    public void testNegative(){
        System.out.println("testNegative");
        Customer c = new Customer(19, false);

        boolean actualResult = cashier.validateCustomer(c);

        Assert.assertFalse(actualResult);
    }

    @Test(groups = {"positiveScenario"})
    public void testBoundary(){
        System.out.println("testBoundary");
        Customer c = new Customer(18, true);

        boolean actualResult = cashier.validateCustomer(c);

        Assert.assertTrue(actualResult);
    }

}
