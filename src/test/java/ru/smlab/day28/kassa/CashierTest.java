package ru.smlab.day28.kassa;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CashierTest {
    //DRY
    /**
     * positive
     */

    @BeforeMethod
    public void setUp() {
        System.out.println("Осуществляем подготовительную работу");
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("Откатываю БД к прежнему состоянию");
    }

    @Test
    public void testPositive(){
        System.out.println("testPositive");
        //подготовка тестовых данных
        Customer c = new Customer(19, true);
        Cashier cashier = new Cashier();

        boolean actualResult = cashier.validateCustomer(c);
        boolean expectedResult = true;

//        Assert.assertEquals(expectedResult, actualResult);
        Assert.assertTrue(actualResult);
    }

    @Test
    public void testNegative(){
        System.out.println("testNegative");
        Customer c = new Customer(19, false);
        Cashier cashier = new Cashier();

        boolean actualResult = cashier.validateCustomer(c);

        Assert.assertFalse(actualResult);
    }

    @Test
    public void testBoundary(){
        System.out.println("testBoundary");
        Customer c = new Customer(18, true);
        Cashier cashier = new Cashier();

        boolean actualResult = cashier.validateCustomer(c);

        Assert.assertTrue(actualResult);
    }

}
