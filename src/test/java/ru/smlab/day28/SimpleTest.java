package ru.smlab.day28;

import org.testng.Assert;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class SimpleTest {

    @Test
    public void testEventHasNotNullFields() {
        String input =
                "Наименование: Школа АТ\n" +
                        "Время: 12.00";

        CalendarEvent event = CalendarImportUtil.convertStringToEvent(input);

        assertNotNull(event, "объект события календаря пуст");
        assertNotNull(event.getTime(), "у события календаря непроинициализировано время");
//        assertNotNull(event.getTime());
//        assertNotNull(event.getName());

    }

//    @Test
//    public void checkEventNotNull(){
//        String input =
//                "Наименование: Школа АТ\n" +
//                        "Время: 12.00";
//        CalendarEvent event = CalendarImportUtil.convertStringToEvent()
//
//    }


    @Test(expectedExceptions = {RuntimeException.class})
    public void testInvalidTimeProvided(){
        String input =
                "Наименование: Школа АТ\n" +
                        "Время: 11200";

        CalendarEvent event = CalendarImportUtil.convertStringToEvent(input);
        //expect Exception
    }

    @Test(expectedExceptions = {RuntimeException.class})
    public void testInvalidData(){
        String input =
                "Наименование: Школа АТВремя: 11.00";

        CalendarEvent event = CalendarImportUtil.convertStringToEvent(input);
    }

    @Test
    public void test3(){
        String input =
                "Наименование: Школа АТ: Апрель\n" +
                        "Время: 11.00";

        CalendarEvent event = CalendarImportUtil.convertStringToEvent(input);

        if (!(event.getName() == "Школа АТ: Апрель")) {
            throw new AssertionError();
        }
    }

}
