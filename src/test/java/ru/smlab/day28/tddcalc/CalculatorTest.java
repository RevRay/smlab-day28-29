package ru.smlab.day28.tddcalc;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CalculatorTest {

    Calculator c;

    @BeforeClass
    public void setUp() {
        c = new Calculator();
    }

    @Test
    public void testAdd() {
        int result = c.add(1, 2);
        Assert.assertEquals(3, result);
    }

    @Test
    public void testDivision() {
        int result = c.divide(8, 2);
        Assert.assertEquals(4, result);
    }

    @Test(expectedExceptions = {ZeroArgumentDetectedException.class})
    public void testDivisionNegative() {
        int result = c.divide(8, 0);
    }
}
