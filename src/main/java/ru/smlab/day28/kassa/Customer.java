package ru.smlab.day28.kassa;

public class Customer {
    private Integer age;
    private Boolean hasPassport;

    public Customer(Integer age, Boolean hasPassport) {
        this.age = age;
        this.hasPassport = hasPassport;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "age=" + age +
                ", hasPassport=" + hasPassport +
                '}';
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getHasPassport() {
        return hasPassport;
    }

    public void setHasPassport(Boolean hasPassport) {
        this.hasPassport = hasPassport;
    }
}
