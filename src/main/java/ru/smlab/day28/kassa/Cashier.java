package ru.smlab.day28.kassa;

public class Cashier {

    public Cashier() {
        System.out.println("Cashier created");
    }

    public boolean validateCustomer(Customer c) {
        System.out.println("Hello! " + c);
        return c.getAge() >= 18 && c.getHasPassport();
    }
}
