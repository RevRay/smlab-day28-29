package ru.smlab.day28;

import java.util.HashMap;
import java.util.Map;

/**
 * Задание в классе 28.0.1
 * Помогаем аутлуку. Наша задача - создать метод,
 * принимающий на вход строку, а на выход - объект СобытиеКалендаря,
 * созданный на основании предложенных данных.
 * Формат входных строк:
 * "Наименование: Школа АТ
 * Время: 12.00"
 */
public class CalendarImportUtil {
    public static CalendarEvent convertStringToEvent(String input) {
        Map<String, String> map = new HashMap<>();

        String[] lines = input.split("\n");
        for (String line : lines) {
            String[] keyAndValue = line.split(": ");
            map.put(keyAndValue[0], keyAndValue[1]);
        }

        String name = map.get("Наименование");
        String time = map.get("Время");

        if (!time.matches("\\d{2}\\.\\d{2}")){
            throw new RuntimeException();
        }

        CalendarEvent ce = null;//new CalendarEvent(name, time);

        System.out.println(ce);

        return ce;
    }
}
